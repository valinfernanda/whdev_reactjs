import React from "react";

const foods = [
  {
    nama: "Batagor",
    harga: 12000,
  },
  {
    nama: "Baso",
    harga: 10000,
  },
  {
    nama: "Ayam Geprek",
    harga: 15000,
  },
  {
    nama: "Cilok",
    harga: 5000,
  },
];

{
  /* Reduce */
}
const total_bayar = foods.reduce((total_harga, food) => {
  return total_harga + food.harga;
}, 0);

const Map = () => {
  return (
    <div>
      <h2>Map Sederhana</h2>
      <ul>
        {foods.map((food, index) => (
          <li>
            {index + 1}. {food.nama} - Harga: {food.harga}
          </li>
        ))}
      </ul>

      {/* Filter */}
      <h2>Map Filter Harga yang lebih dari 10.000</h2>
      <ul>
        {foods
          .filter((food) => food.harga > 10000)
          .map((food, index) => (
            <li>
              {index + 1}. {food.nama} - Harga: {food.harga}
            </li>
          ))}
      </ul>
      <h3>Total harga: {total_bayar}</h3>
    </div>
  );
};

export default Map;
