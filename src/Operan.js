import React, { Component } from "react";

export default class Operan extends Component {
  //   gantiMakanan(makanan_baru) {
  //     this.setState({
  //       makanan: makanan_baru,
  //     });
  //   }
  render() {
    const { operfood, gantiMakanan } = this.props;
    return (
      <div>
        <h2>Operan State yang menjadi Props : {operfood}</h2>
        <button onClick={() => gantiMakanan("Soto")}>Ganti Makanan</button>
      </div>
    );
  }
}

//dalam state, ga cuma state yang bisa dioper, tapi function juga bisa dioper
